<?php
/*
Les add_action permet d'ajouter une fonction qui sera exécutée
par Wordpress dans un contexte spécifique.
Le premier argument est le contexte dans lequel elle sera exécutée,
aussi appelé hook, on voit les valeurs possibles sur la doc (https://codex.wordpress.org/Plugin_API/Action_Reference)
Le deuxième argument est le nom de la fonction en string
*/
add_action('wp_enqueue_scripts', 'parent_style');
/**
 * fonction pour charger le style du thème parent dans le thème enfant
 */
function parent_style() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

add_action('customize_register', 'customizer_pimp');
/**
 * Fonction pour ajouter une nouvelle option éditable par le client dans
 * le customizer
 */
function customizer_pimp( $wp_customize ) {
    //On crée une nouvelle section dans le customizer
    $wp_customize->add_section('my_section', [
        'title' => 'My Section',
        'priority' => 30
    ]);
    //On crée un nouveau setting qui représente une valeur modifiable
    $wp_customize->add_setting('para_color', [
        'default' => 'red',
        'transport' => 'refresh'
    ]);
    //On crée un control qui représente le champ qui sera présent dans 
    //le customizer, on doit lui dire dans quel section ira ce champ
    //et quel setting ce champ modifiera
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'para_color_control', [
        'section' => 'my_section',
        'label' => 'Change Para color',
        'settings' => 'para_color'
    ]));
}
add_action('wp_head', 'customizer_style');
/**
 * Fonction qui crée une balise style en assignant les valeurs du 
 * customizer dans les propriétés voulues
 */
function customizer_style() {
    ?>
    <style>
        p {
            /*On peut choper un setting avec la fonction get_theme_mod
            suivi de son nom*/
            background-color: <?php echo get_theme_mod('para_color'); ?> ;
        }
    </style>

    <?php
}

